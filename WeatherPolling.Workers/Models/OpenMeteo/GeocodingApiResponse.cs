﻿using System.Text.Json.Serialization;

namespace WeatherPolling.Workers.Models.OpenMeteo;

public class GeocodingApiResponse
{
    [JsonPropertyName("results")]
    public LocationData[]? Locations { get; set; }
    
    [JsonPropertyName("generationtime_ms")]
    public float GenerationTime { get; set; }
}