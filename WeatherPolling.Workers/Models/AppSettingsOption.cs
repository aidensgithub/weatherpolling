﻿using System.Text.Json.Serialization;

namespace WeatherPolling.Workers.Models;

public class AppSettingsOption
{
    public List<LocationSettings> Locations { get; set; }
}

public class LocationSettings
{
    public string Alias { get; set; }
    public string Latitude { get; set; }
    public string Longitude { get; set; }
    public int PollIntervalMinutes { get; set; }
}