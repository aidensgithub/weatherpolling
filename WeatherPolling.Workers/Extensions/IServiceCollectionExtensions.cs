﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using WeatherPolling.Workers.API;
using WeatherPolling.Workers.Services;

namespace WeatherPolling.Workers.Extensions;

public static class IServiceCollectionExtensions
{
    public static IServiceCollection AddOpenMeteoService(this IServiceCollection services)
    {
        services.TryAddSingleton<OpenMeteoGeocodingApi>();
        services.TryAddSingleton<OpenMeteoWeatherForecastApi>();
        services.TryAddSingleton<OpenMeteoService>();
        return services;
    }
}