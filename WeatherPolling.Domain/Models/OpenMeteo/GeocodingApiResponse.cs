﻿using System.Text.Json.Serialization;

namespace WeatherPolling.Domain.Models.OpenMeteo;

public class GeocodingApiResponse
{
    [JsonPropertyName("results")]
    public LocationData[]? Locations { get; set; }
    
    [JsonPropertyName("generationtime_ms")]
    public float GenerationTime { get; set; }
}