﻿using MongoDB.Driver;
using WeatherPolling.Domain.Models;

namespace WeatherPolling.Domain.Repositories;

public class WeatherDataRepository : IWeatherDataRepository
{
    private readonly IMongoCollection<WeatherModel> _collection;

    public WeatherDataRepository(IMongoDatabase database)
    {
        _collection = database.GetCollection<WeatherModel>("WeatherCollections");
    }
     public async Task<List<WeatherModel>> GetManyAsync()
    {
        var results = await _collection.FindAsync(_ => true);
        return results.ToList();
    }
    
    public async Task CreateAsync(WeatherModel weatherModel)
    {
        await _collection.InsertOneAsync(weatherModel);
    }
    
    public async Task CreateManyAsync(List<WeatherModel> weatherModels)
    {
        await _collection.InsertManyAsync(weatherModels);
    }

    public Task UpsertManyAsync(List<WeatherModel> weatherModels, CancellationToken cancellationToken = default)
    {
        var bulkOps = new List<WriteModel<WeatherModel>>();
        foreach (var document in weatherModels)
        {
            var filters = Builders<WeatherModel>.Filter.Eq("Alias", document.Alias)
                          & Builders<WeatherModel>.Filter.Eq("Latitude", document.Latitude)
                          & Builders<WeatherModel>.Filter.Eq("Longitude", document.Longitude);
            var update = Builders<WeatherModel>.Update
                .SetOnInsert("Alias", document.Alias)
                .SetOnInsert("Latitude", document.Latitude)
                .SetOnInsert("Longitude", document.Longitude)
                .Set("HourlyTemperature", document.HourlyTemperature);
            bulkOps.Add(new UpdateOneModel<WeatherModel>(filters, update) {IsUpsert = true});
        }

        return _collection.BulkWriteAsync(bulkOps, cancellationToken: cancellationToken);
    }

    public Task UpsertAsync(WeatherModel weatherModel, CancellationToken token)
    {
        var filters = Builders<WeatherModel>.Filter.Eq("Alias", weatherModel.Alias)
                      & Builders<WeatherModel>.Filter.Eq("Latitude", weatherModel.Latitude)
                      & Builders<WeatherModel>.Filter.Eq("Longitude", weatherModel.Longitude);
        var update = Builders<WeatherModel>.Update
            .SetOnInsert("Alias", weatherModel.Alias)
            .SetOnInsert("Latitude", weatherModel.Latitude)
            .SetOnInsert("Longitude", weatherModel.Longitude)
            .Set("HourlyTemperature", weatherModel.HourlyTemperature);
        return _collection.UpdateOneAsync(filters, update, new UpdateOptions(){IsUpsert = true}, token);
    }
}