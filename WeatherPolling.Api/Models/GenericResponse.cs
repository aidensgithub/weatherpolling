﻿using System.Text.Json.Serialization;

namespace WeatherPolling.Api.Models;

public class GenericResponse
{
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? ErrorMessage { get; set; }
    public Dictionary<string, string>? ErrorDetails { get; set; }
}