﻿namespace WeatherPolling.Workers.Models.OpenMeteo;

public class GeocodingApiRequest
{
  
    public string Name { get; }
    
    public string Language { get; }
    
    public string Format { get; }
    
    public int Count { get; }

    public GeocodingApiRequest(string city, string language, int count)
    {
        Name = city;
        Language = language;
        Format = "json";
        Count = count;
    }

    public GeocodingApiRequest(string city)
    {
        Name = city;
        Language = "en";
        Format = "json";
        Count = 100;
    }
}