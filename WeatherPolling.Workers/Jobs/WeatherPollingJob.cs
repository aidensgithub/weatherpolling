﻿using Microsoft.Extensions.Caching.Distributed;
using Quartz;
using WeatherPolling.Domain.Extensions;
using WeatherPolling.Domain.Repositories;
using WeatherPolling.Workers.Services;

namespace WeatherPolling.Workers.Jobs;

public class WeatherPollingJob : IJob
{
    private readonly OpenMeteoService _service;
    private readonly IWeatherDataRepository _repository;
    private readonly IDistributedCache _cache;
    private readonly ILogger<WeatherPollingJob> _logger;

    public WeatherPollingJob(OpenMeteoService service, IWeatherDataRepository repository, IDistributedCache cache,
        ILogger<WeatherPollingJob> logger)
    {
        _service = service;
        _repository = repository;
        _cache = cache;
        _logger = logger;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        try
        {
            
            var dataMap = context.MergedJobDataMap;
            var city = dataMap.GetString("alias");
            var lat = dataMap.GetString("latitude");
            var lon = dataMap.GetString("longitude");
            if (city == null || lat == null || lon == null) throw new JobExecutionException("City or Latitude or Longitude was not given.");
            var response = await _service.QueryByCoordinatesAsync(lat, lon);
            if (response == null)
                throw new JobExecutionException($"Weather data couldn't be queried for the city. {city}");
            var converted = response.MapToWeatherModel(city);
            _logger.LogInformation("Weather data for {City} has been queried successfully.", city);
            await _repository.UpsertAsync(converted, context.CancellationToken);
            _logger.LogInformation("Weather data for {City} has been saved to Mongo successfully.", city);
            await _cache.CreateOrUpdateRecordAsync(city, converted);
            _logger.LogInformation("Weather data for {City} has been saved to Redis successfully.", city);
        }
        catch (JobExecutionException e)
        {
            _logger.LogError("Something went wrong while executing the job. {ErrorMessage}", e.Message);
        }
    }
}