﻿using WeatherPolling.Workers.API;
using WeatherPolling.Workers.Models.OpenMeteo;

namespace WeatherPolling.Workers.Services;

public class OpenMeteoService
{
    private readonly OpenMeteoGeocodingApi _geocodingApi;
    private readonly OpenMeteoWeatherForecastApi _weatherApi;

    public OpenMeteoService(OpenMeteoGeocodingApi geocodingApi,
        OpenMeteoWeatherForecastApi weatherApi)
    {
        _geocodingApi = geocodingApi;
        _weatherApi = weatherApi;
    }

    /// <summary>
    /// Get weather forecast by city
    /// </summary>
    /// <param name="city"></param>
    /// <param name="timezone"></param>
    /// <param name="hourlyOptions">By default temperature_2m</param>
    /// <returns></returns>
    public async Task<WeatherForecastApiResponse?> QueryByCityAsync(string city, string? timezone = null,
        List<HourlyOptionsEnum>? hourlyOptions = null)
    {
        // Get coordinates by city
        var geocodingResponse = await _geocodingApi.QueryGeocodingByCityAsync(city);
        if (geocodingResponse?.Locations == null)
            return null;
        //Get weather forecast by coordinates
        var request = new WeatherForecastApiRequest
        {
            Latitude = geocodingResponse.Locations[0].Latitude,
            Longitude = geocodingResponse.Locations[0].Longitude,
        };
        if (!string.IsNullOrWhiteSpace(timezone))
            request.Timezone = timezone;
        if (hourlyOptions != null)
            request.HourlyOptions.AddRange(hourlyOptions);

        return await _weatherApi.GetWeatherForecastAsync(request);
    }

    public async Task<WeatherForecastApiResponse?> QueryByCoordinatesAsync(string latitude, string longitude,
        string? timezone = null, List<HourlyOptionsEnum>? hourlyOptions = null)
    {
        //Get weather forecast by coordinates
        var request = new WeatherForecastApiRequest
        {
            Latitude = latitude,
            Longitude = longitude,
        };
        if (!string.IsNullOrWhiteSpace(timezone))
            request.Timezone = timezone;
        if (hourlyOptions != null)
            request.HourlyOptions.AddRange(hourlyOptions);

        return await _weatherApi.GetWeatherForecastAsync(request);
    }
}