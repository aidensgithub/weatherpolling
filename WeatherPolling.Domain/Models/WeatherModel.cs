﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WeatherPolling.Domain.Models;

public class WeatherModel
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    [BsonIgnoreIfDefault]
    public string Id { get; set; }

    public string Alias { get; set; }

    public double Latitude { get; set; }
    public double Longitude { get; set; }

    public List<WeatherModelHourlyTemperature> HourlyTemperature { get; set; }
}

public class WeatherModelHourlyTemperature
{
    public string Time { get; set; }
    public double Temperature { get; set; }
}