﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace WeatherPolling.Domain.Extensions;

public static class IServiceCollectionExtensions
{
    public static IServiceCollection AddMongoDb(this IServiceCollection services, IConfiguration config, string configSectionName)
    {
        services.TryAddSingleton<IMongoClient>(c => new MongoClient(config.GetConnectionString(configSectionName)));
        services.TryAddScoped<IMongoDatabase>(c =>
            c.GetRequiredService<IMongoClient>().GetDatabase(config.GetSection(configSectionName)["Database"]));
        return services;
    }
}