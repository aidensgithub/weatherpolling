﻿namespace WeatherPolling.Workers.Models.OpenMeteo;

public class WeatherForecastApiRequest
{
    public string Latitude { get; set; }
    public string Longitude { get; set; }
    public string? Timezone { get; set; }

    public List<HourlyOptionsEnum> HourlyOptions { get; set; } = new() {HourlyOptionsEnum.Temperature2m};
}