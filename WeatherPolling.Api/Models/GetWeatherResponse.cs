﻿using WeatherPolling.Domain.Models;

namespace WeatherPolling.Api.Models;

public class GetWeatherResponse : GenericResponse
{
    public WeatherModel? Payload { get; set; }
}