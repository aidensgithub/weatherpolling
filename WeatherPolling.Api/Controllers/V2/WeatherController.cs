using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Microsoft.Extensions.Caching.Distributed;
using WeatherPolling.Api.Models;
using WeatherPolling.Domain.Extensions;
using WeatherPolling.Domain.Models;

namespace WeatherPolling.Api.Controllers.V2;

[ApiController]
[ApiVersion("2.0")]
[Route("v{version:apiVersion}/weather-api")]
public class WeatherController : ControllerBase
{
    private readonly IDistributedCache _cache;
    private readonly ILogger<WeatherController> _logger;

    public WeatherController(IDistributedCache cache, ILogger<WeatherController> logger)
    {
        _cache = cache;
        _logger = logger;
    }

    [MapToApiVersion("2.0")]
    [OutputCache(Duration = 1,
        VaryByRouteValueNames = new[] {"city"})]
    [HttpGet("locations/{city}/temperature")]
    [Produces("application/json")]
    public async Task<ActionResult<GetWeatherResponse>> GetWeatherByCity(string city)
    {
        try
        {
            var weather = await _cache.GetRecordAsync<WeatherModel>(city);
            if (weather == null) throw new Exception("Weather not found for the given City");
            return new GetWeatherResponse {Payload = weather};
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, JsonSerializer
                .Serialize(new GenericResponse() {ErrorMessage = ex.Message}));
        }
    }
}