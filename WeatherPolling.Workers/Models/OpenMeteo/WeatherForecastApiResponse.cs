﻿using System.Text.Json.Serialization;
using WeatherPolling.Domain.Models;

namespace WeatherPolling.Workers.Models.OpenMeteo;

public class WeatherForecastApiResponse
{
    [JsonPropertyName("latitude")] public double Latitude { get; set; }

    [JsonPropertyName("longitude")] public double Longitude { get; set; }

    [JsonPropertyName("generationtime_ms")]
    public double? GenerationtimeMs { get; set; }

    [JsonPropertyName("utc_offset_seconds")]
    public int? UtcOffsetSeconds { get; set; }

    [JsonPropertyName("timezone")] public string Timezone { get; set; }

    [JsonPropertyName("timezone_abbreviation")]
    public string TimezoneAbbreviation { get; set; }

    [JsonPropertyName("elevation")] public double? Elevation { get; set; }

    [JsonPropertyName("hourly_units")] public WeatherForecastApiResponseHourlyUnits HourlyUnits { get; set; }

    [JsonPropertyName("hourly")] public WeatherForecastApiResponseHourly Hourly { get; set; }

    public WeatherModel MapToWeatherModel(string city)
    {
        return new WeatherModel
        {
            Alias = city,
            Latitude = Latitude,
            Longitude = Longitude,
            HourlyTemperature = Hourly.Time.Zip(Hourly.Temperature2m, (k, v) => new WeatherModelHourlyTemperature
            {
                Time = k,
                Temperature = v
            }).ToList()
        };
    }
}

public class WeatherForecastApiResponseHourly
{
    [JsonPropertyName("time")] public IReadOnlyList<string> Time { get; set; }

    [JsonPropertyName("temperature_2m")] public IReadOnlyList<double> Temperature2m { get; set; }
}

public class WeatherForecastApiResponseHourlyUnits
{
    [JsonPropertyName("time")] public string Time { get; set; }

    [JsonPropertyName("temperature_2m")] public string Temperature2m { get; set; }
}