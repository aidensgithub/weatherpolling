using System.Net;
using Polly;
using Polly.Contrib.WaitAndRetry;
using Quartz;
using WeatherPolling.Domain.Extensions;
using WeatherPolling.Domain.Repositories;
using WeatherPolling.Workers.Extensions;
using WeatherPolling.Workers.Jobs;
using WeatherPolling.Workers.Models;


// Builder
var builder = Host.CreateApplicationBuilder(args);
// Services
var services = builder.Services;
// Configuration
var configuration = builder.Configuration;
configuration.AddEnvironmentVariables();
configuration.AddUserSecrets<Program>();
configuration.AddJsonFile($"appsettings.json", optional: true, reloadOnChange: true);
// Redis
services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = configuration.GetConnectionString("Redis");
});
// Mongo
services.AddMongoDb(configuration, "MongoDb");
services.AddScoped<IWeatherDataRepository, WeatherDataRepository>();
// Polly rate limiting policies
var policies = Policy<HttpResponseMessage>.Handle<HttpRequestException>()
    .OrResult(x => x.StatusCode is HttpStatusCode.TooManyRequests or HttpStatusCode.ServiceUnavailable
        or HttpStatusCode.InternalServerError or HttpStatusCode.Forbidden)
    .WaitAndRetryAsync(Backoff.DecorrelatedJitterBackoffV2(TimeSpan.FromSeconds(5), 3));
// Rate Limited Named Client
services.AddHttpClient("OpenMeteoGeocodingApiClient",
        client => { client.BaseAddress = new Uri("https://geocoding-api.open-meteo.com/v1/search"); })
    .AddPolicyHandler(policies);
services.AddHttpClient("OpenMeteoWeatherForecastApiClient",
    client => { client.BaseAddress = new Uri("https://api.open-meteo.com/v1/forecast"); }).AddPolicyHandler(policies);
// OpenMeteo
services.AddOpenMeteoService();
// Quartz
services.AddQuartz(q =>
{
    q.SchedulerId = "WeatherPullersScheduler";
    q.UseMicrosoftDependencyInjectionJobFactory();
    q.UseInMemoryStore();
    q.UseDefaultThreadPool(tp => { tp.MaxConcurrency = 3; });
    var locations = builder.Configuration.Get<AppSettingsOption>()?.Locations;
    if (locations is null)
        throw new Exception("No locations found in configuration file");

    foreach (var location in locations)
    {
        var jobKey = new JobKey($"{location.Alias}_{location.Latitude}_{location.Longitude}", "WeatherPullers");
        q.AddJob<WeatherPollingJob>(opts =>
        {
            opts.WithIdentity(jobKey);
            opts.UsingJobData("latitude", location.Latitude);
            opts.UsingJobData("longitude", location.Longitude);
            opts.UsingJobData("alias", location.Alias);
        });
        var interval = location.PollIntervalMinutes != 0 ? location.PollIntervalMinutes : 5;
        q.AddTrigger(opts => opts
            .ForJob(jobKey)
            .WithIdentity($"{jobKey}_trigger")
            .WithDailyTimeIntervalSchedule(x =>
                x.WithInterval(interval, IntervalUnit.Minute))
            .StartAt(DateBuilder.EvenSecondDate(DateTimeOffset.UtcNow.AddSeconds(3))));
    }
});
services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);

// Building
var host = builder.Build();
// Running
host.Run();