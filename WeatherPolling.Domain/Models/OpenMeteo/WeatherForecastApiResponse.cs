﻿using System.Text.Json.Serialization;

namespace WeatherPolling.Domain.Models.OpenMeteo;

public record WeatherForecastApiResponse
(
    [property: JsonPropertyName("latitude")]
    double? Latitude,
    [property: JsonPropertyName("longitude")]
    double? Longitude,
    [property: JsonPropertyName("utc_offset_seconds")]
    int? UtcOffsetSeconds,
    [property: JsonPropertyName("timezone")]
    string Timezone,
    [property: JsonPropertyName("timezone_abbreviation")]
    string TimezoneAbbreviation,
    [property: JsonPropertyName("hourly_units")]
    WeatherForecastApiResponseHourlyUnits HourlyUnits,
    [property: JsonPropertyName("hourly")] WeatherForecastApiResponseHourly Hourly
);

public record WeatherForecastApiResponseHourly(
    [property: JsonPropertyName("time")] IReadOnlyList<string> Time,
    [property: JsonPropertyName("temperature_2m")]
    IReadOnlyList<double?> Temperature2m
);

public record WeatherForecastApiResponseHourlyUnits(
    [property: JsonPropertyName("time")] string Time,
    [property: JsonPropertyName("temperature_2m")]
    string Temperature2m
);