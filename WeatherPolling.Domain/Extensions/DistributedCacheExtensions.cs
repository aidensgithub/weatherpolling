﻿using System.Text.Json;
using Microsoft.Extensions.Caching.Distributed;

namespace WeatherPolling.Domain.Extensions;

public static class DistributedCacheExtensions
{
    public static async Task SetRecordAsync<T>(this IDistributedCache cache, string key, T data,
        TimeSpan? absoluteExpireTime = null,
        TimeSpan? unusedExpireTime = null)
    {
        var options = new DistributedCacheEntryOptions();
        options.AbsoluteExpirationRelativeToNow =
            absoluteExpireTime ??
            TimeSpan.FromDays(
                60); // The task says Controller should not make request to the OpenMeteo API. So I set the default value to 60 days. The right way to do would be at least to pull from MongoDB. Or Pull from OpenMeteo API upon expiration of cache but with Polly circuit breaker to not throttle OpenMeteo API
        options.SlidingExpiration = unusedExpireTime ?? TimeSpan.FromDays(
            60);
        var jsonData = JsonSerializer.Serialize(data);
        await cache.SetStringAsync(key, jsonData, options);
    }

    public static async Task<T?> GetRecordAsync<T>(this IDistributedCache cache, string key)
    {
        var jsonData = await cache.GetStringAsync(key);
        return jsonData is null ? default(T) : JsonSerializer.Deserialize<T>(jsonData);
    }

    public static async Task RemoveRecordAsync(this IDistributedCache cache, string key)
    {
        await cache.RemoveAsync(key);
    }

    public static async Task CreateOrUpdateRecordAsync<T>(this IDistributedCache cache, string key, T data,
        TimeSpan? absoluteExpireTime = null,
        TimeSpan? unusedExpireTime = null)
    {
        var jsonData = await cache.GetRecordAsync<T>(key);
        if (jsonData is null)
        {
            await cache.SetRecordAsync(key, data, absoluteExpireTime, unusedExpireTime);
        }
        else
        {
            await cache.RemoveRecordAsync(key);
            await cache.SetRecordAsync(key, data, absoluteExpireTime, unusedExpireTime);
        }
    }
}