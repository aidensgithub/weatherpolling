﻿namespace WeatherPolling.Domain.Models.OpenMeteo;

public class WeatherForecastApiRequest
{
    public float Latitude { get; set; }
    public float Longitude { get; set; }
    public string? Timezone { get; set; }

    public List<HourlyOptionsEnum> HourlyOptions { get; set; } = new() {HourlyOptionsEnum.Temperature2m};
}