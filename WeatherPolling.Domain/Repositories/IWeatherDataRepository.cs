﻿using WeatherPolling.Domain.Models;

namespace WeatherPolling.Domain.Repositories;

public interface IWeatherDataRepository
{
    Task<List<WeatherModel>> GetManyAsync();
    Task CreateAsync(WeatherModel weatherModel);
    Task CreateManyAsync(List<WeatherModel> weatherModels);
    Task UpsertManyAsync(List<WeatherModel> weatherModels, CancellationToken cancellationToken = default);
    Task UpsertAsync(WeatherModel weatherModel, CancellationToken token);
}