﻿## Go to solution folder and hit:
`docker compose up -d`

## Redis Insight URL:
http://localhost:8001/

## Swagger URL:
http://localhost:8080/swagger/index.html

If there is any errors on Redis while launching:
Try to clean all previously created volumes for this docker-compose.