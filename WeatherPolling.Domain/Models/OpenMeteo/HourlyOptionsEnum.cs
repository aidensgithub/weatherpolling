﻿namespace WeatherPolling.Domain.Models.OpenMeteo;

public enum HourlyOptionsEnum
{
    Temperature2m,
    ApparentTemperature
}

public static class HourlyEnumOptionsExtensions
{
    public static string ToFastString(this HourlyOptionsEnum hourlyOption)
    {
        return hourlyOption switch
        {
            HourlyOptionsEnum.Temperature2m => "temperature_2m",
            HourlyOptionsEnum.ApparentTemperature => "apparent_temperature",
            _ => "temperature_2m"
        };
    }
}
