﻿using System.Text.Json;
using WeatherPolling.Workers.Models.OpenMeteo;

namespace WeatherPolling.Workers.API;

public class OpenMeteoWeatherForecastApi
{
    private readonly IHttpClientFactory _clientFactory;
    private readonly IConfigurationSection _configuration;
    private readonly ILogger<OpenMeteoWeatherForecastApi> _logger;

    public OpenMeteoWeatherForecastApi(IHttpClientFactory clientFactory, IConfiguration configuration,
        ILogger<OpenMeteoWeatherForecastApi> logger)
    {
        _clientFactory = clientFactory;
        _logger = logger;
        _configuration = configuration.GetSection("OpenMeteoApi");
    }

    public async Task<WeatherForecastApiResponse?> GetWeatherForecastAsync(WeatherForecastApiRequest requestParams)
    {
        try
        {
            using (var client = _clientFactory.CreateClient("OpenMeteoWeatherForecastApiClient"))
            {
                var baseUrl = client.BaseAddress;
                var response = await client.GetAsync(BuildUrl(requestParams, baseUrl));
                if (response.IsSuccessStatusCode)
                {
                    return await JsonSerializer.DeserializeAsync<WeatherForecastApiResponse>(
                        await response.Content.ReadAsStreamAsync(),
                        new JsonSerializerOptions() {PropertyNameCaseInsensitive = true});
                }
            }
        }
        catch (Exception e)
        {
            _logger.LogError("Something went wrong while querying the OpenMeteo Weather Forecast API. {ErrorMessage}",
                e.Message);
        }

        return null;
    }

    private Uri BuildUrl(WeatherForecastApiRequest requestParams, Uri baseUrl)
    {
        var url = new UriBuilder(baseUrl)
        {
            Query = $"latitude={requestParams.Latitude}&longitude={requestParams.Longitude}"
        };
        if (requestParams.Timezone != null) url.Query += $"&timezone={requestParams.Timezone}";
        foreach (var each in requestParams.HourlyOptions)
            url.Query += $"&hourly={each.ToFastString()}";
        return url.Uri;
    }
}