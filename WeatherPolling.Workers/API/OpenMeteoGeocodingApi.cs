﻿using System.Text.Json;
using WeatherPolling.Workers.Models.OpenMeteo;

namespace WeatherPolling.Workers.API;

public class OpenMeteoGeocodingApi
{
    private readonly IHttpClientFactory _clientFactory;
    private readonly IConfigurationSection _configuration;
    private readonly ILogger<OpenMeteoGeocodingApi> _logger;

    public OpenMeteoGeocodingApi(IHttpClientFactory clientFactory, IConfiguration configuration,
        ILogger<OpenMeteoGeocodingApi> logger)
    {
        _clientFactory = clientFactory;
        _logger = logger;
        _configuration = configuration.GetSection("OpenMeteoApi");
    }

    public async Task<GeocodingApiResponse?> QueryGeocodingByCityAsync(string city)
    {
        var requestParams = new GeocodingApiRequest(city);
        return await GetGeocodingAsync(requestParams);
    }

    private async Task<GeocodingApiResponse?> GetGeocodingAsync(GeocodingApiRequest requestParams)
    {
        try
        {
            using (var client = _clientFactory.CreateClient("OpenMeteoGeocodingApiClient"))
            {
                var baseUrl = client.BaseAddress;
                var response = await client.GetAsync(BuildUrl(requestParams, baseUrl));
                if (response.IsSuccessStatusCode)
                {
                    return await JsonSerializer.DeserializeAsync<GeocodingApiResponse>(
                        await response.Content.ReadAsStreamAsync(),
                        new JsonSerializerOptions() {PropertyNameCaseInsensitive = true});
                }
            }
        }
        catch (Exception e)
        {
            _logger.LogError("Something went wrong while querying the OpenMeteo Geocoding API. {ErrorMessage}",
                e.Message);
        }

        return null;
    }

    private Uri BuildUrl(GeocodingApiRequest requestParams, Uri baseUrl)
    {
        
        var url = new UriBuilder(baseUrl);
        if (requestParams.Name == null)
            throw new ArgumentNullException(nameof(requestParams.Name),
                "City name cannot be null, please reinitialize the request parameters object.");
        url.Query =
            $"q={requestParams.Name}&lang={requestParams.Language}&format={requestParams.Format}&limit={requestParams.Count}";
        return url.Uri;
    }
}